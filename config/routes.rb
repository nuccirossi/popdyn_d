Rails.application.routes.draw do
  
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  get '/logout', to: 'sessions#destroy'

  resources :users
  get 'population_dynamics/home'

  get 'population_dynamics/charts'

  resources :relations
  resources :countries
  resources :indicators
  
  root 'population_dynamics#home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
