require 'test_helper'

class RelationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @relation = relations(:one)
  end

  test "should get index" do
    get relations_url
    assert_response :success
  end

  test "should get new" do
    get new_relation_url
    assert_response :success
  end

  test "should create relation" do
    assert_difference('Relation.count') do
      post relations_url, params: { relation: { countrycode: @relation.countrycode, indicatorcode: @relation.indicatorcode, y2007: @relation.y2007, y2008: @relation.y2008, y2009: @relation.y2009, y2010: @relation.y2010, y2011: @relation.y2011, y2012: @relation.y2012, y2013: @relation.y2013, y2014: @relation.y2014, y2015: @relation.y2015, y2016: @relation.y2016 } }
    end

    assert_redirected_to relation_url(Relation.last)
  end

  test "should show relation" do
    get relation_url(@relation)
    assert_response :success
  end

  test "should get edit" do
    get edit_relation_url(@relation)
    assert_response :success
  end

  test "should update relation" do
    patch relation_url(@relation), params: { relation: { countrycode: @relation.countrycode, indicatorcode: @relation.indicatorcode, y2007: @relation.y2007, y2008: @relation.y2008, y2009: @relation.y2009, y2010: @relation.y2010, y2011: @relation.y2011, y2012: @relation.y2012, y2013: @relation.y2013, y2014: @relation.y2014, y2015: @relation.y2015, y2016: @relation.y2016 } }
    assert_redirected_to relation_url(@relation)
  end

  test "should destroy relation" do
    assert_difference('Relation.count', -1) do
      delete relation_url(@relation)
    end

    assert_redirected_to relations_url
  end
end
