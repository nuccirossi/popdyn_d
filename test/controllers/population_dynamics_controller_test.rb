require 'test_helper'

class PopulationDynamicsControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get population_dynamics_home_url
    assert_response :success
  end

  test "should get charts" do
    get population_dynamics_charts_url
    assert_response :success
  end

end
