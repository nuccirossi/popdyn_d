namespace :countries do
  desc "TODO"
  task seed_countries: :environment do
    require 'csv'
    Country.destroy_all
    CSV.foreach("countries.csv", headers: true) do |row|
      Country.create!(
      code: row["Code"], 
      name: row["Name"])
    end
    p "Countries imported."
  end

end
