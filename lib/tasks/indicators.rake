namespace :indicators do
  desc "TODO"
  task seed_indicators: :environment do
    require 'csv'
    Indicator.destroy_all
    CSV.foreach("indicators.csv", headers: true) do |row|
      Indicator.create!(
        code: row["Code"], 
        name: row["Name"], 
        definition: row["Definition"], 
        source: row["Source"])
    end
    p "Indicators imported."
  end

end
