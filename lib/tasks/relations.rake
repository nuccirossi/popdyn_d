namespace :relations do
  desc "TODO"
  task seed_relations: :environment do
    require 'csv'
    Relation.destroy_all
    CSV.foreach("relations.csv", headers: true) do |row|
      Relation.create!(
        countrycode: row["Country Code"], 
        indicatorcode: row["Indicator Code"], 
        y2007: row["YR2007"],
        y2008: row["YR2008"],
        y2009: row["YR2009"],
        y2010: row["YR2010"],
        y2011: row["YR2011"],
        y2012: row["YR2012"],
        y2013: row["YR2013"],
        y2014: row["YR2014"],
        y2015: row["YR2015"],
        y2016: row["YR2016"])
    end
    p "Relations imported."
  end

end
