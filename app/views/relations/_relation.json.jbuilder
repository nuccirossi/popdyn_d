json.extract! relation, :id, :countrycode, :indicatorcode, :y2007, :y2008, :y2009, :y2010, :y2011, :y2012, :y2013, :y2014, :y2015, :y2016, :created_at, :updated_at
json.url relation_url(relation, format: :json)
