json.extract! indicator, :id, :code, :name, :definition, :source, :created_at, :updated_at
json.url indicator_url(indicator, format: :json)
