class RelationsController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, with: :redirect_if_not_found
  before_action :set_relation, only: [:show, :edit, :update, :destroy]

  # GET /relations
  # GET /relations.json
  def index
    @relations = if params[:term]
        Relation.where("LOWER(countrycode) LIKE ? OR LOWER(indicatorcode) LIKE ?", "%#{params[:term].downcase}%", "%#{params[:term].downcase}%")
      else
        Relation.all
    end
    @countries = Country.all
    @indicators = Indicator.all
  end

  # GET /relations/1
  # GET /relations/1.json
  def show
    @countries = Country.all
    @indicators = Indicator.all
  end

  # GET /relations/new
  def new
    @relation = Relation.new
    @countries = Country.all
    @indicators = Indicator.all
  end

  # GET /relations/1/edit
  def edit
  end

  # POST /relations
  # POST /relations.json
  def create
    @relation = Relation.new(relation_params)

    respond_to do |format|
      if @relation.save
        format.html { redirect_to @relation, notice: 'Relation was successfully created.' }
        format.json { render :show, status: :created, location: @relation }
      else
        format.html { render :new }
        format.json { render json: @relation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /relations/1
  # PATCH/PUT /relations/1.json
  def update
    respond_to do |format|
      if @relation.update(relation_params)
        format.html { redirect_to @relation, notice: 'Relation was successfully updated.' }
        format.json { render :show, status: :ok, location: @relation }
      else
        format.html { render :edit }
        format.json { render json: @relation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /relations/1
  # DELETE /relations/1.json
  def destroy
    @relation.destroy
    respond_to do |format|
      format.html { redirect_to relations_url, notice: 'Relation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_relation
      @relation = Relation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def relation_params
      params.require(:relation).permit(:countrycode, :indicatorcode, :y2007, :y2008, :y2009, :y2010, :y2011, :y2012, :y2013, :y2014, :y2015, :y2016, :term)
    end
end
