class CountriesController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, with: :redirect_if_not_found
  before_action :set_country, only: [:show, :edit, :update, :destroy]

  # GET /countries
  # GET /countries.json
  def index
      @countries = if params[:term]
          Country.where("LOWER(code) LIKE ? OR LOWER(name) LIKE ?", "%#{params[:term].downcase}%", "%#{params[:term].downcase}%")
      else
          Country.all
      end
  end

  # GET /countries/1
  # GET /countries/1.json
  def show
    @populationtot = Relation.find_by countrycode: @country.code, indicatorcode: 'SP.POP.TOTL'
    @populationf = Relation.find_by countrycode: @country.code, indicatorcode: 'SP.POP.TOTL.FE.IN'
    @populationm = Relation.find_by countrycode: @country.code, indicatorcode: 'SP.POP.TOTL.MA.IN'
    @popanf = [@populationf.y2015, @populationf.y2014, @populationf.y2013, @populationf.y2012, @populationf.y2011, @populationf.y2010, @populationf.y2009, @populationf.y2008, @populationf.y2007]
    @popanm = [@populationm.y2015, @populationm.y2014, @populationm.y2013, @populationm.y2012, @populationm.y2011, @populationm.y2010, @populationm.y2009, @populationm.y2008, @populationm.y2007]
    @pop14f = Relation.find_by countrycode: @country.code, indicatorcode: 'SP.POP.0014.FE.IN'
    @pop1564f = Relation.find_by countrycode: @country.code, indicatorcode: 'SP.POP.1564.FE.IN'
    @pop65f = Relation.find_by countrycode: @country.code, indicatorcode: 'SP.POP.65UP.FE.IN'
    @popcompf = [@pop14f.y2015, @pop1564f.y2015, @pop65f.y2015]
    @pop14m = Relation.find_by countrycode: @country.code, indicatorcode: 'SP.POP.0014.MA.IN'
    @pop1564m = Relation.find_by countrycode: @country.code, indicatorcode: 'SP.POP.1564.MA.IN'
    @pop65m = Relation.find_by countrycode: @country.code, indicatorcode: 'SP.POP.65UP.MA.IN'
    @popcompm = [@pop14m.y2015, @pop1564m.y2015, @pop65m.y2015]
  end

  # GET /countries/new
  def new
    @country = Country.new
  end

  # GET /countries/1/edit
  def edit
  end

  # POST /countries
  # POST /countries.json
  def create
    @country = Country.new(country_params)

    respond_to do |format|
      if @country.save
        format.html { redirect_to @country, notice: 'Country was successfully created.' }
        format.json { render :show, status: :created, location: @country }
      else
        format.html { render :new }
        format.json { render json: @country.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /countries/1
  # PATCH/PUT /countries/1.json
  def update
    respond_to do |format|
      if @country.update(country_params)
        format.html { redirect_to @country, notice: 'Country was successfully updated.' }
        format.json { render :show, status: :ok, location: @country }
      else
        format.html { render :edit }
        format.json { render json: @country.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /countries/1
  # DELETE /countries/1.json
  def destroy
    @country.destroy
    respond_to do |format|
      format.html { redirect_to countries_url, notice: 'Country was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_country
      @country = Country.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def country_params
      params.require(:country).permit(:code, :name, :term)
    end
end
