class PopulationDynamicsController < ApplicationController
  skip_before_action :logged_in?
  def home
  end

  def charts
    @populationtot = Relation.find_by countrycode: 'WLD', indicatorcode: 'SP.POP.TOTL'
    @populationf = Relation.find_by countrycode: 'WLD', indicatorcode: 'SP.POP.TOTL.FE.IN'
    @populationm = Relation.find_by countrycode: 'WLD', indicatorcode: 'SP.POP.TOTL.MA.IN'
    @popanf = [@populationf.y2007, @populationf.y2008, @populationf.y2009, @populationf.y2010, @populationf.y2011, @populationf.y2012, @populationf.y2013, @populationf.y2014, @populationf.y2015]
    @popanm = [@populationm.y2007, @populationm.y2008, @populationm.y2009, @populationm.y2010, @populationm.y2011, @populationm.y2012, @populationm.y2013, @populationm.y2014, @populationm.y2015]
    @admr = Relation.find_by countrycode: 'WLD', indicatorcode: 'SP.DYN.AMRT'
    @admrf = Relation.find_by countrycode: 'WLD', indicatorcode: 'SP.DYN.AMRT.FE'
    @admrm = Relation.find_by countrycode: 'WLD', indicatorcode: 'SP.DYN.AMRT.MA'
    @admranf = [@admrf.y2007, @admrf.y2008, @admrf.y2009, @admrf.y2010, @admrf.y2011, @admrf.y2012, @admrf.y2013, @admrf.y2014, @admrf.y2015]
    @admranm = [@admrm.y2007, @admrm.y2008, @admrm.y2009, @admrm.y2010, @admrm.y2011, @admrm.y2012, @admrm.y2013, @admrm.y2014, @admrm.y2015]
    @lifeexp = Relation.find_by countrycode: 'WLD', indicatorcode: 'SP.DYN.LE00.IN'
    @lifeexpf = Relation.find_by countrycode: 'WLD', indicatorcode: 'SP.DYN.LE00.FE.IN'
    @lifeexpm = Relation.find_by countrycode: 'WLD', indicatorcode: 'SP.DYN.LE00.MA.IN'
    @liexanf = [@lifeexpf.y2007, @lifeexpf.y2008, @lifeexpf.y2009, @lifeexpf.y2010, @lifeexpf.y2011, @lifeexpf.y2012, @lifeexpf.y2013, @lifeexpf.y2014, @lifeexpf.y2015]
    @liexanm = [@lifeexpm.y2007, @lifeexpm.y2008, @lifeexpm.y2009, @lifeexpm.y2010, @lifeexpm.y2011, @lifeexpm.y2012, @lifeexpm.y2013, @lifeexpm.y2014, @lifeexpm.y2015]
    @pop14f = Relation.find_by countrycode: 'WLD', indicatorcode: 'SP.POP.0014.FE.IN'
    @pop1564f = Relation.find_by countrycode: 'WLD', indicatorcode: 'SP.POP.1564.FE.IN'
    @pop65f = Relation.find_by countrycode: 'WLD', indicatorcode: 'SP.POP.65UP.FE.IN'
    @popcompf = [@pop14f.y2015, @pop1564f.y2015, @pop65f.y2015]
    @pop14m = Relation.find_by countrycode: 'WLD', indicatorcode: 'SP.POP.0014.MA.IN'
    @pop1564m = Relation.find_by countrycode: 'WLD', indicatorcode: 'SP.POP.1564.MA.IN'
    @pop65m = Relation.find_by countrycode: 'WLD', indicatorcode: 'SP.POP.65UP.MA.IN'
    @popcompm = [@pop14m.y2015, @pop1564m.y2015, @pop65m.y2015]
  end
end
