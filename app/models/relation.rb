class Relation < ApplicationRecord
    validates :countrycode, :indicatorcode, presence: true
end
