class Indicator < ApplicationRecord
    validates :code, :name, presence: true
end
