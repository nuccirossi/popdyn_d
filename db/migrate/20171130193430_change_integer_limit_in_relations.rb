class ChangeIntegerLimitInRelations < ActiveRecord::Migration[5.1]
  def change
    change_column :relations, :y2007, :integer, limit: 8
    change_column :relations, :y2008, :integer, limit: 8
    change_column :relations, :y2009, :integer, limit: 8
    change_column :relations, :y2010, :integer, limit: 8
    change_column :relations, :y2011, :integer, limit: 8
    change_column :relations, :y2012, :integer, limit: 8
    change_column :relations, :y2013, :integer, limit: 8
    change_column :relations, :y2014, :integer, limit: 8
    change_column :relations, :y2015, :integer, limit: 8
    change_column :relations, :y2016, :integer, limit: 8
  end
end
