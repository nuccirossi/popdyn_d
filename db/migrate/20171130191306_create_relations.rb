class CreateRelations < ActiveRecord::Migration[5.1]
  def change
    create_table :relations do |t|
      t.string :countrycode, null: false
      t.string :indicatorcode, null: false
      t.integer :y2007
      t.integer :y2008
      t.integer :y2009
      t.integer :y2010
      t.integer :y2011
      t.integer :y2012
      t.integer :y2013
      t.integer :y2014
      t.integer :y2015
      t.integer :y2016

      t.timestamps
    end
    add_index :relations, :countrycode
    add_index :relations, :indicatorcode
  end
end
