class CreateIndicators < ActiveRecord::Migration[5.1]
  def change
    create_table :indicators do |t|
      t.string :code, null: false
      t.string :name
      t.string :definition
      t.string :source

      t.timestamps
    end
    add_index :indicators, :code, unique: true
  end
end