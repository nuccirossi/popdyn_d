# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171201173622) do

  create_table "countries", force: :cascade do |t|
    t.string "code", null: false
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_countries_on_code", unique: true
  end

  create_table "indicators", force: :cascade do |t|
    t.string "code", null: false
    t.string "name"
    t.string "definition"
    t.string "source"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_indicators_on_code", unique: true
  end

  create_table "relations", force: :cascade do |t|
    t.string "countrycode", null: false
    t.string "indicatorcode", null: false
    t.integer "y2007", limit: 8
    t.integer "y2008", limit: 8
    t.integer "y2009", limit: 8
    t.integer "y2010", limit: 8
    t.integer "y2011", limit: 8
    t.integer "y2012", limit: 8
    t.integer "y2013", limit: 8
    t.integer "y2014", limit: 8
    t.integer "y2015", limit: 8
    t.integer "y2016", limit: 8
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["countrycode"], name: "index_relations_on_countrycode"
    t.index ["indicatorcode"], name: "index_relations_on_indicatorcode"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
